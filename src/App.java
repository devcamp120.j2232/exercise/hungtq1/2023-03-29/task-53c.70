import javax.sound.midi.MidiChannel;

import models.*;
public class App {
    public static void main(String[] args) throws Exception {
        Circle circle = new Circle(5);
        System.out.println("Circle: "+ circle.toString());

        ResizableCircle resizableCircle = new ResizableCircle(5);
        System.out.println("Resized Circle: "+ resizableCircle.toString());

        double radius1 = resizableCircle.resize(10);
        System.out.println("Circle After Resizing: "+ resizableCircle.toString());
        Circle circle2 = new Circle(radius1);
        circle2.getArea();
        circle2.getPerimeter();
        System.out.println("Circle 2: "+ circle2.toString());

    }
}
